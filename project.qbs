Product {
    name: "web-minify"
    Group{
        name: "Code - Executable"
        prefix: "./"
        excludeFiles: "/**/internal/**/*"
        files: [
            "/**/web*.py",
        ]
    }
    Group{
        name: "Code - Library"
        prefix: "web_minify"
        excludeFiles: "/**/internal/**/*"
        files: [
            "/**/*.html",
            "/**/*.jinja2",
            "/**/*.py",
            "/**/*.txt",
        ]
    }
    Group{
        name: "Tests"
        prefix: "tests"
        excludeFiles: "/**/internal/**/*"
        files: [
            "/**/*.py",
            "/**/*.html",
            "/**/*.jinja2",
            "/**/*.jpeg",
            "/**/*.json",
            "/**/*.png",
            "/**/*.svg",
        ]
    }
    Group{
        name: "Meta"
        prefix: "./"
        excludeFiles: "/**/internal/**/*"
        files: [
            "*.qbs",
            ".*ignore",
            ".gitlab*",
            "Dockerfile",
            "Makefile*",
            "README.*",
            "tpl_README.*",
            "VERSION",
            "design/*.svg",
            "requirements/*",
            "resources/**/*",
            "docker-*.yaml",
            "local-*.sh",
            "setup.*",
            ".env",
        ]
    }
    Group{
        name: "Design"
        prefix: "design"
        excludeFiles: "/**/internal/**/*"
        files: [
            "/**/*.jpeg",
            "/**/*.json",
            "/**/*.png",
            "/**/*.svg",
        ]
    }
}
