#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
from contextlib import contextmanager
import html
import logging
import os
import re
import sys

logger = logging.getLogger(__name__)


space_re = re.compile(r"^(\s*)", re.MULTILINE)

# default_formatter = 'html5lib'
default_formatter = "html.parser"
too_long_line = 1000


@contextmanager
def isolate_import_path():
	"""
	Context manager to temporarily remove the current working directory from sys.path.
	"""
	# Save the original sys.path
	original_sys_path = sys.path.copy()
	
	# Get the current working directory
	current_path = os.getcwd()
	
	# Remove the current working directory from sys.path
	if current_path in sys.path:
		sys.path.remove(current_path)
	
	try:
		# Execute the block of code within the context
		yield
	finally:
		# Restore the original sys.path
		sys.path = original_sys_path

def html_beautify(raw, settings, encoding=None, formatter=default_formatter, indents=4):
	stripped = raw.strip()
	if len(stripped) <= 0:
		return True, stripped, None
	s = BeautifulSoup(raw, formatter)
	formatted = s.prettify(encoding=encoding)
	formatted = formatted.strip() + "\n"
	parts = formatted.split("\n")
	for part in parts:
		if len(part) > too_long_line:
			return True, raw, [f"Something wrong in html output! A line was generated with a length above the limit of {too_long_line} characters"]
	return True, formatted, None
	# spaced = space_re.sub(r"\1" * indents, formatted)
	# return spaced.strip() + "\n", None


def html_beautify_bob(raw, settings, encoding=None, formatter=default_formatter, indents=4):
	ret = raw
	try:
		with isolate_import_path():
			from html5print import HTMLBeautifier
			ret = HTMLBeautifier.beautify(raw, indents).strip() + "\n"
	except Exception as e:
		False, raw, [f"Error beautifying HTML: {e}"]
	return True, ret, None
