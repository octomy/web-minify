# The project's group name in domain form (hyphen allowed, underscore not)
FK_PROJECT_GROUP_BASE_NAME_D:=octomy
# The project's base name in domain form (hyphen allowed, underscore not)
FK_PROJECT_BASE_NAME_D:=web-minify
# The cli executable for generating help output in readme
FK_CLI_EXECUTABLE:=web-minify.py

# The latest general firstkiss makefile. See 
include Makefile.fk


